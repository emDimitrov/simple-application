package com.picklesolutions.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.picklesolutions.UsersService;

public class TestUsersService {

	@Test
	public void testCount() {
		UsersService service = new UsersService();
		
		assertEquals(10, service.count());
	}
	
	@Test
	public void testUserAuthentication() {
		UsersService service = new UsersService();
		
		assertEquals(true, service.authenticateUser("admin", "adminpass"));
		assertEquals(false, service.authenticateUser("coke", "adminpass"));
		assertEquals(false, service.authenticateUser("admin", "coke"));
		assertEquals(false, service.authenticateUser("emil", "emilpass"));
	}
}
